#!/bin/zsh

echo "Installing oh-my-zsh.";

# Create directory to work in.
mkdir /tmp/ohmyzsh -p;
cd /tmp/ohmyzsh

# Downloading repo.
yes | sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)";

echo "DONE";
