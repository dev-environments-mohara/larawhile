#!/bin/zsh

echo "Installing and configuring PHP 7.1.";

# Add repo.
apt-get update -y && add-apt-repository ppa:ondrej/php;
apt-get update -y;

# Install php7.1 and modules to have.
apt-get install -y php7.1 php7.1-cli php7.1-common \
  php7.1-json php7.1-opcache php7.1-mysql \
  php7.1-mbstring php7.1-mcrypt php7.1-zip \
  php7.1-fpm php7.1-xml php7.1-curl php7.1-gd \
  php7.1-xdebug;

sh ./scripts/configure/php71.sh;

echo "DONE"