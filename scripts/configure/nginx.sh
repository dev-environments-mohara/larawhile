#!/bin/zsh

echo "Configuring nginx."

# Copying base service.
service nginx.service stop;

cat ./configuration/nginx/default > /etc/nginx/sites-available/default;
ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default;

service nginx.service start;