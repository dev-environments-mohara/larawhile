#!/bin/zsh

echo "Configuring composer."

# Clean up
echo 'export PATH="$PATH:$HOME/.config/composer/vendor/bin"' >> ~/.zshrc;

sh ./scripts/configure/composer.sh;
