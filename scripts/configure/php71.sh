#!/bin/zsh

echo "Configuring nginx."

# Configure php.
mkdir /tmp/php -p;
cd /tmp/php;
sed "s/\;cgi\.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php/7.1/cli/php.ini > ./php.ini \
  && mv ./php.ini /etc/php/7.1/cli/php.ini;

# Configure FPM to listen over a TCP socker instead.
service php7.1-fpm stop;

sed "s/listen = \/run\/php\/php7.1-fpm.sock/listen = devcontainer:9000/g" /etc/php/7.1/fpm/pool.d/www.conf > ./www.conf \
  && sudo mv ./www.conf /etc/php/7.1/fpm/pool.d/;

service php7.1-fpm start;