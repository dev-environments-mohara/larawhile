# larawhile

The ideal of a perfect world where you have the whole environment set up as containers for Laravel... not yet, but soon.

## Launch container ##
The container depends on the UID and GID from your local and for some reason I could not get them into the script without you setting the variables on launch time.

Here are a list of the variables that need to be set:
1. USER_UID
2. USER_GID
3. USER_NAME
4. DISPLAY
5. ROOT_PASSWORD

After setting the files above, you can either run the `services/root.yml` file or using the `launch.sh` shell script. See the issue about the GID variable.

## Tools installed ##
### OS ###
1. wget
2. zsh
3. git

### PHP-related ###
1. php
2. php-apache
3. composer

## Known issues ##
* The GID variable is not set in all shells. In my case I use zsh which does have it set but when running my scripts with `bash` (or `sh` when bash is set as the default shell) it is not set and the Dockerfile fails to build.

    To fix this you can either manually export the GID variable before launching the script or change to a shell that does have this. [Here](https://www.howtoforge.com/linux-csh-command/) is an example of how to change your default shell.

    UPDATE: I changed the launch sript to run in zsh isntead. Not sure about how this will affect you if you do not have it installed so just keep that in mind.

## Nginx configuration ##
The configuration for the nginx server can be done through the `configuration/nginx` directory. Currently there are templates being used and this allows you to set a variable in the docker-compose service and then use that variable in the server. What it is currently being used for is to set the NGINX_HOST which is the address of the devcontainer that has the server and the fpm server on there.

In the `.template` file you just use the environment variable in the ${} directive. and this will then compile all of the files using the variable that you have set in the environment field in the docker service.

## Notes ##
* To change the username, go to the launch.sh file or export your own USER_NAME variable when running the docker-compose file.

## Future work ##
* Split the services out into separate files.