#!/bin/zsh

# Setting variables for building
export USER_UID=$(echo $UID)
export USER_GID=$(echo $GID)
export USER_NAME=starfighter
export DISPLAY=$(echo $DISPLAY)
export ROOT_PASSWORD='toets'

# Change the permissions for the .docker directory
sudo chmod 777 ./ -R

# Launch the root docker-compose file in the root directory.
docker-compose up -d
